{
  description = "Declarative system";

  inputs = {
  nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable"; #nixos-unstable
  };

  outputs = { self, nixpkgs }:
  let version = builtins.substring 0 8 self.lastModifiedDate;
  in {
  packages.aarch64-darwin = {
    default = self.packages.aarch64-darwin.personal;
    personal =
    let pkgs = nixpkgs.legacyPackages.aarch64-darwin;
    in pkgs.buildEnv {
    name = "personal";
    paths = with pkgs; [
      ctags
      git
      jq
      loc
      lua-language-server
      neovim
      pandoc
      ripgrep
      starship
      tmux
      tree-sitter
      vim
    ];

    extraOutputsToInstall = [ "man" "doc" ];
    };
  };
  };
}
