# system-declaration

To install, run:

```
nix profile install git+https://codeberg.org/erikwastaken/system-declaration.git
```

To upgrade the packages, run:

```
nix profile upgrade [package]
```
